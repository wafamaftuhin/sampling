========
Sampling
========

Project for the interaction between ASE and rdkit.

Install
=======

The package depends on the libraries
[openbabel]() and
[rdkit](https://www.rdkit.org/docs/Install.html).
See the installation instructions there.

We need python3 bindings to be able to work with ase.
In ubuntu::

  sudo apt install python3-openbabel
  sudo apt install python3-rdkit

Examples
========

See an initial example for the creation of conformers in
doc/
