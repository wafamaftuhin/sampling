from rdkit.Chem import AllChem
from ase import io

from sampling.translate import AtomsCreator, atoms_to_rdkit
from sampling.mmff94 import MMFF94
from sampling.optimize.internal import Internal


class Conformers(list):
    def __init__(self, atoms):
        self.atoms = atoms
        self._mol = atoms_to_rdkit(atoms)
        print('__init__ mol', self._mol)
        super().append(atoms)

    def add(self, number):
        """Add a given number of conformers"""
        # see https://www.rdkit.org/docs/Cookbook.html#conformer-generation-with-etkdg
        params = AllChem.ETKDG()
        params.useSmallRingTorsions = True
        AllChem.EmbedMultipleConfs(
            self._mol, numConfs=number, params=params)

        for i, conf in enumerate(self._mol.GetConformers()):
            atoms = AtomsCreator().rdkit(self._mol,
                                         conf.GetPositions())
            super().append(atoms)

    def write(self, trajname):
        with io.Trajectory(trajname, 'w') as traj:
            for atoms in self:
                traj.write(atoms)

    def relax(self, fmax=0.05, optimizer=Internal,
              initialize=None):
        """Relax all conformers

        fmax: maximal force (ignored)
        optimizer: Optimizer to use, default Internal
        initialize: function to initialize the atoms object,
          must return the modified atoms object
        """
        for atoms in self:
            if initialize is None:
                atoms.calc = MMFF94()
            else:
                atoms = initialize(atoms)
            opt = optimizer(atoms)
            opt.run(fmax=fmax)

    def sort(self):
        """Sort conformers by energy"""
        super().sort(key=lambda atoms: atoms.get_potential_energy())
