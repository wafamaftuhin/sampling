import pytest
from ase.build import molecule
from ase.optimize import BFGS

from sampling.mmff94 import MMFF94
from sampling.optimize.internal import Internal


def test_MMFF94():
    atoms = molecule('CH2OCH2')
    atoms.calc = MMFF94()

    E0 = atoms.get_potential_energy()

    opt = BFGS(atoms)
    opt.run(fmax=0.01)
    assert E0 > atoms.get_potential_energy()


def test_relaxtion():
    formula = 'CH2OCH2'
    atoms0 = molecule(formula)
    atoms0.calc = MMFF94()

    fmax = 0.05
    opt = BFGS(atoms0)
    opt.run(fmax=fmax)

    atoms1 = molecule(formula)
    atoms1.calc = MMFF94()
    opt = Internal(atoms1)
    opt.run(fmax=fmax)

    assert atoms0.get_potential_energy() == pytest.approx(
        atoms1.get_potential_energy(), 1e-2)


def test_change_atoms():
    atoms0 = molecule('CH2OCH2')
    atoms0.calc = MMFF94()

    atoms0.get_potential_energy()
    id0 = id(atoms0.calc.mol)

    atoms0[3].position[2] += 0.1
    atoms0.get_potential_energy()
    # we changed position only, so mol should be the same
    assert id0 == id(atoms0.calc.mol)

    atoms0[3].symbol = 'F'
    atoms0.get_potential_energy()
    # we changed numbers, so mol should be different
    assert id0 != id(atoms0.calc.mol)
