import numpy as np
from ase.optimize import BFGS
from ase.calculators.dftb import Dftb

from sampling.translate import AtomsCreator
from sampling.conformers import Conformers
from sampling.mmff94 import MMFF94


def test_build():
    atoms = AtomsCreator().smiles('CC(C)Cc1ccc(cc1)C(C)C(=O)O')

    confs = Conformers(atoms)
    confs.add(1)
    assert len(confs) == 2  # includes initial structure
    confs.add(1)
    assert len(confs) == 3
    confs.write('conf_initial.traj')

    try:
        confs.sort()
    except RuntimeError:
        pass
    else:
        raise 'Sorting should fail as we did not assign calc yet'

    # energy before relaxation
    ene0 = []
    for conf in confs:
        conf.calc = MMFF94()
        ene0.append(conf.get_potential_energy())
    ene0 = np.array(ene0)

    confs.relax()
    ene1 = np.array([conf.get_potential_energy()
                     for conf in confs])
    assert (ene1 < ene0).all()
    confs.write('conf_relaxed.traj')

    confs.sort()
    ene2 = np.array([conf.get_potential_energy()
                     for conf in confs])
    assert (ene2[1:] - ene2[:1] > 0).all()
    confs.write('conf_sorted.traj')


def test_dftb():
    atoms = AtomsCreator().smiles('CCCC')

    confs = Conformers(atoms)
    confs.add(1)
    confs.write('confs_initial.traj')

    confs.relax()
    confs.sort()
    confs.write('confs_mmff94.traj')

    def initialize(atoms):
        atoms.center(vacuum=3)
        atoms.calc = Dftb()
        return atoms

    confs.relax(optimizer=BFGS, initialize=initialize)
    confs.write('confs_dftb.traj')

    E = np.array([conf.get_potential_energy() for conf in confs])
    confs.sort()
    Esort = np.array([conf.get_potential_energy() for conf in confs])
    assert (sorted(E) == Esort).all()
